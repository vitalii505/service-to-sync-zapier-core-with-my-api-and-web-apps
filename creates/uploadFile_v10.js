const http = require('https');
const FormData = require('form-data');
const mime = require('mime-types')

const makeDownloadStream = (url) =>
    new Promise((resolve, reject) => {
        http
            .request(url, (res) => {
                res.pause();
                resolve(res);
            })
            .on('error', reject)
            .end();
    });

const perform = async (z, bundle) => {
    const stream = await makeDownloadStream(bundle.inputData.file, z);

    const responseToken = await z.request({
        method: 'POST',
        url: 'https://cognito-idp.us-east-1.amazonaws.com/',
        headers: {
            'Content-Type': 'application/x-amz-json-1.1',
            'x-amz-target': 'AWSCognitoIdentityProviderService.InitiateAuth',
            'x-amz-user-agent': 'aws-amplify/0.1.x js'
        },
        body: {
            AuthFlow: "USER_PASSWORD_AUTH",
            ClientId: process.env.CLIENT_ID,
            AuthParameters: {
                USERNAME: process.env.LOGIN,
                PASSWORD: process.env.PASSWORD
            },
            ClientMetadata: {},
        },
    });
    const access_token = "Bearer " + responseToken.json.AuthenticationResult.AccessToken;

    const getSignature = await z.request({
        url: 'https://dev.prezentor.com/api/filearchive/sign_s3',
        method: 'GET',
        params: {
            s3_object_folder: "staging/presentations/" + '61fbb8584f7506001e135a2c',
            s3_object_name: bundle.inputData.filename,
            s3_object_type: mime.lookup(bundle.inputData.filename) || 'application/pdf'
        },
        headers: {
            "Authorization": access_token,
            withCredentials: true,
        },
    });

    const signConfig = getSignature.data;
    const formData = new FormData();
    formData.append('key', `staging/presentations/${'61fbb8584f7506001e135a2c'}${signConfig.s3Filename}`)
    formData.append('AWSAccessKeyId', process.env.AWSACCESSKEYID)
    formData.append('acl', 'public-read')
    formData.append('policy', signConfig.s3Policy)
    formData.append('signature', signConfig.s3Signature)
    formData.append('Content-Type', mime.lookup(bundle.inputData.filename) || 'application/pdf')
    formData.append('filename', signConfig.s3Filename)
    formData.append('file', stream)
    stream.resume();
    // const contentLength = await formData.getLengthSync();
    await z.request({
        url: 'https://s3-eu-west-1.amazonaws.com/dev.prezentor.com',
        method: 'POST',
        body: formData,
        maxContentLength: Infinity,
        maxBodyLength: Infinity,
        headers: {
            ...formData.getHeaders(),
            'Content-Length': 200000
        }
    })
    const objFile = {
        hidden: "false",
        parent: '61fbb8584f7506001e135a2c',
        title: bundle.inputData.filename,
        url: 'https://cdndev.prezentor.com/' + `staging/presentations/${'61fbb8584f7506001e135a2c'}${signConfig.s3Filename}`,
    }
    const response = await z.request({
        url: 'https://dev.prezentor.com/api/filearchive/',
        method: 'POST',
        body: objFile,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': access_token,
        }
    })
    return response.data;
};

module.exports = {
    key: 'uploadFile_v10',
    noun: 'File',
    display: {
        label: 'Upload File v10',
        description: 'Uploads a file. Only works on zapier-platform-core v10+.',
    },
    operation: {
        inputFields: [
            { key: 'filename', required: true, type: 'string', label: 'Filename' },
            { key: 'file', required: true, type: 'file', label: 'File' },
        ],
        perform,
        sample: {
            id: 1,
            filename: 'example.pdf',
            file: 'SAMPLE FILE',
        },
    },
};