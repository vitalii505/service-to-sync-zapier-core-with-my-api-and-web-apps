'use strict';

const getUserTokens = async (z, bundle) => {
  const responseToken = await z.request({
    method: 'POST',
    url: 'https://cognito-idp.us-east-1.amazonaws.com/',
    headers: {
        'Content-Type': 'application/x-amz-json-1.1',
        'x-amz-target': 'AWSCognitoIdentityProviderService.InitiateAuth',
        'x-amz-user-agent': 'aws-amplify/0.1.x js'
    },
    body: {
        AuthFlow: "USER_PASSWORD_AUTH",
        ClientId: process.env.CLIENT_ID,
        AuthParameters: {
            USERNAME: process.env.LOGIN,
            PASSWORD: process.env.PASSWORD
        },
        ClientMetadata: {},
    },
  });
  const access_token = responseToken.json.AuthenticationResult.AccessToken;
  const refresh_token = responseToken.json.AuthenticationResult.RefreshToken;
  return {
    access_token: access_token,
    refresh_token: refresh_token,
  };
};

const getAccessToken = async (z, bundle) => {
  const responseByTokens = await getUserTokens(z, bundle);
  return {
    access_token: responseByTokens.access_token,
    refresh_token: responseByTokens.refresh_token,
  };
};

const refreshAccessToken = async (z, bundle) => {
  const responseByTokens = await getUserTokens(z, bundle);
  return {
    access_token: responseByTokens.access_token,
    refresh_token: responseByTokens.refresh_token,
  };
};

const includeBearerToken = async (request, z, bundle) => {
  request.headers.Authorization = `Bearer ${bundle.authData.access_token}`;
  return request;
};

const test = async (z, bundle) => {
  const response = await z.request({ 
    url: 'http://localhost/api/users/me',
    method: 'GET', 
    headers: {
      Authorization: `Bearer ${bundle.authData.access_token}`, 
    }
  })
  return response
}

module.exports = {
  config: {
    type: 'oauth2',
    oauth2Config: {
      authorizeUrl: {
        url: 'http://localhost:3002/authorize',
        method: 'GET',
        params: {
          response_type: 'code',
          client_id: '{{process.env.CLIENT_ID}}',
          redirect_uri: '{{bundle.inputData.redirect_uri}}',
          scope: 'openid',
          state: '{{bundle.inputData.state}}',
          code: '{{process.env.CODE}}'
        },
      },
      getAccessToken,
      refreshAccessToken,
      autoRefresh: true,
    },

    fields: [],
    test,

    connectionLabel: '{{json}}',
  },
  befores: [includeBearerToken],
  afters: [],
};