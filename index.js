const {
  config: authentication,
  befores = [],
  afters = [],
} = require('./authentication');

const createdActionResource = require("./resources/created_action");
const hydrators = require('./hydrators');
const newFile = require('./triggers/newFile');
const uploadFileV10 = require('./creates/uploadFile_v10');

module.exports = {
  // This is just shorthand to reference the installed dependencies you have.
  // Zapier will need to know these before we can upload.
  version: require('./package.json').version,
  platformVersion: require('zapier-platform-core').version,
  
  authentication,

  beforeRequest: [...befores],
  afterResponse: [...afters],
  
  hydrators,

  // If you want your trigger to show up, you better include it here!
  triggers: {
    [newFile.key]: newFile,
  },

  // If you want your searches to show up, you better include it here!
  searches: {},

  // If you want your creates to show up, you better include it here!
  creates: {
    [uploadFileV10.key]: uploadFileV10,
  },

  resources: {
    [createdActionResource.key]: createdActionResource,
  },
};
